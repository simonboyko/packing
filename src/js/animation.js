document.addEventListener('DOMContentLoaded', function () {
  'use strict';

  jQuery(document).ready(function() {


    //['.titer__heading', '.titer__descr', '.titer__button']

    // titer
    // $('.titer__heading, .titer__descr, .titer__button').addClass("hidden");
    // setTimeout(function () {
    //   $('.titer__image').viewportChecker({
    //     classToAdd: 'visible animated bounceInDown',
    //     offset: 10,
    //   });
    // }, 500);
    // setTimeout(function () {
    //   $('.titer__photo.-sales').viewportChecker({
    //     classToAdd: 'visible animated fadeIn slideInLeft',
    //     offset: 100,
    //   });
    // }, 1000);
    // setTimeout(function () {
    //   $('.titer__photo.-marketing').viewportChecker({
    //     classToAdd: 'visible animated fadeIn slideInRight',
    //     offset: 100,
    //   });
    // }, 1200);
    // setTimeout(function () {
    //   $('.titer__heading, .titer__descr, .titer__button, .titer__upperTitle').viewportChecker({
    //     classToAdd: 'visible animated fadeIn',
    //     offset: 100,
    //   });
    // }, 1800);


    // Титр
    $('.titer__heading').viewportChecker({
      classToAdd: 'visible animated fadeIn',
      offset: 100,
    });
    $('.titer__button').viewportChecker({
      classToAdd: 'visible animated fadeIn',
      offset: 100,
    });

    // Марсель говорит
    $('.marselSay__image').addClass("hidden");
    $('.marselSay__blockquote').addClass("hidden");
    $('.marselSay__image').viewportChecker({
      classToAdd: 'visible animated fadeInLeft',
      offset: 100,
    });
    $('.marselSay__blockquote').viewportChecker({
      classToAdd: 'visible animated fadeIn',
      offset: 200,
    });

    // Допустим, ваш товар или услугу
    $('.goodsSearch__item').addClass("hidden");
    $('.goodsSearch__calculator').addClass("hidden");
    $('.goodsSearch__item:nth-child(1)').viewportChecker({
      classToAdd: 'visible animated fadeIn slideInUp',
      offset: 100,
    });
    $('.goodsSearch__item:nth-child(2)').viewportChecker({
      classToAdd: 'visible animated fadeIn slideInUp',
      offset: 200,
    });
    $('.goodsSearch__item:nth-child(3)').viewportChecker({
      classToAdd: 'visible animated fadeIn slideInUp',
      offset: 300,
    });
    $('.goodsSearch__item:nth-child(4)').viewportChecker({
      classToAdd: 'visible animated fadeIn slideInUp',
      offset: 400,
    });
    $('.goodsSearch__calculator').viewportChecker({
      classToAdd: 'visible animated fadeInUp',
      offset: 100,
    });

    // Что такое серьезная упаковка бизнеса
    $('.seriosPacking__content').addClass("hidden");
    $('.seriosPacking__content').viewportChecker({
      classToAdd: 'visible animated fadeIn',
      offset: 100,
    });

    // Нарпавления
    $('.businessBranches__item').addClass("hidden");
    $('.businessBranches__item').viewportChecker({
      classToAdd: 'visible animated fadeIn',
      offset: 100,
    });

    // Позиционирование
    $('.position__whatOffer').addClass("hidden");
    $('.position__whatAdvantages').addClass("hidden");
    $('.position__result').addClass("hidden");
    $('.position__whatOffer').viewportChecker({
      classToAdd: 'visible animated fadeIn',
      offset: 100,
    });
    $('.position__whatAdvantages').viewportChecker({
      classToAdd: 'visible animated fadeIn',
      offset: 100,
    });
    $('.position__result').viewportChecker({
      classToAdd: 'visible animated fadeInDown',
      offset: 100,
    });

    // Оболочка
    $('.shell__tabContentSide').addClass("hidden");
    $('.shell__tabContentSide').viewportChecker({
      classToAdd: 'visible animated fadeInRight',
      offset: 100,
    });

    // Техническая оптимизация сайта
    $('.techOpti__blockquote').addClass("hidden");
    $('.techOpti__blockquote').viewportChecker({
      classToAdd: 'visible animated fadeIn',
      offset: 100,
    });

    // Поток клиента
    $('.clientStream__item').addClass("hidden");
    $('.clientStream__item:nth-child(1)').viewportChecker({
      classToAdd: 'visible animated fadeIn',
      offset: 100,
    });
    $('.clientStream__item:nth-child(2)').viewportChecker({
      classToAdd: 'visible animated fadeIn',
      offset: 200,
    });
    $('.clientStream__item:nth-child(3)').viewportChecker({
      classToAdd: 'visible animated fadeIn',
      offset: 300,
    });

    // Аналитика
    $('.analytics__item').addClass("hidden");
    $('.analytics__item:nth-child(1)').viewportChecker({
      classToAdd: 'visible animated fadeIn',
      offset: 100,
    });
    $('.analytics__item:nth-child(2)').viewportChecker({
      classToAdd: 'visible animated fadeIn',
      offset: 200,
    });
    $('.analytics__item:nth-child(3)').viewportChecker({
      classToAdd: 'visible animated fadeIn',
      offset: 300,
    });

    // Отдел продаж
    $('.salesDepartment__body').addClass("hidden");
    $('.salesDepartment__body').viewportChecker({
      classToAdd: 'visible animated fadeInUp',
      offset: 100,
    });
    $('.salesDepartment__button').addClass("hidden");
    $('.salesDepartment__button').viewportChecker({
      classToAdd: 'visible animated fadeInUp',
      offset: 100,
    });

    // Отдел продаж
    $('.loyalty__item').addClass("hidden");
    $('.loyalty__item:nth-child(1)').viewportChecker({
      classToAdd: 'visible animated fadeInUp',
      offset: 100,
    });
    $('.loyalty__item:nth-child(2)').viewportChecker({
      classToAdd: 'visible animated fadeInUp',
      offset: 200,
    });
    $('.loyalty__item:nth-child(3)').viewportChecker({
      classToAdd: 'visible animated fadeInUp',
      offset: 300,
    });

    // А теперь серьезно
    $('.seriously__inner').addClass("hidden");
    $('.seriously__inner').viewportChecker({
      classToAdd: 'visible animated fadeInRight',
      offset: 100,
    });

  });
});