// function isMobile() {
//   var width = window.innerWidth;
//   if (width < 760) {
//     return true;
//   } else {
//     return false;
//   }
// }
//
// function aboutSpeakers() {
//   var wrapper = document.querySelector('.aboutSpeakers');
//   var resItems = document.querySelectorAll('.aboutSpeakers__item:nth-child(n + 5)');
//
//
//   if (isMobile()) {
//     createButton(wrapper, resItems);
//     toggleItems(resItems);
//   }
//
//
//   function createButton(wrapper, items) {
//     var btn = document.createElement('div');
//     btn.className = 'aboutSpeakers__buttonWrp';
//
//     // btn.classList.add('results__buttonWrp');
//     btn.innerHTML = '<span class="aboutSpeakers__button button">Смотреть все цифры</span>';
//
//     wrapper.appendChild(btn);
//
//     btn.addEventListener('click', function (e) {
//       toggleItems(items);
//       toggleBtnText(e.target);
//     });
//   }
//
//   function toggleBtnText(btn) {
//
//     if (!btn.show || btn.show === 'hidden') {
//
//       btn.innerHTML = 'Свернуть цифры';
//       btn.show = 'show';
//
//     } else {
//
//       btn.innerHTML = 'Смотреть все цифры';
//       btn.show = 'hidden';
//
//     }
//
//   }
//
//   function toggleItems(items) {
//     if (items[0].getAttribute('hidden')) {
//
//       for (var i = 0; i < items.length; i++) {
//         items[i].removeAttribute('hidden');
//       }
//
//     } else {
//
//       for (var i = 0; i < items.length; i++) {
//         items[i].setAttribute('hidden', null);
//       }
//
//     }
//
//   }
// }
//
// function tariffs() {
//   var items = document.querySelectorAll('.tariffs__item');
//   var bodyClass = 'tariffs__body';
//   var spoilerBtnClass = 'tariffs__spoilerBtn';
//
//
//   // создаем кнопку для
//   if (isMobile()) {
//     for (var i = 0; i < items.length; i++) {
//       createButton(items[i], i);
//     }
//
//   }
//
//   function createButton(wrapper, index) {
//     var body = wrapper.querySelector('.' + bodyClass);
//
//     if (body) {
//
//       body.classList.add('-close');
//       wrapper.spoilerState = 'close';
//
//       var btnWrp = document.createElement('div');
//       btnWrp.className = 'tariffs__spoilerWrp';
//
//       // btn.classList.add('results__buttonWrp');
//       btnWrp.innerHTML = '<span class="' + spoilerBtnClass + '">Что включено</span>';
//
//       var btn = btnWrp.querySelector('.' + spoilerBtnClass);
//       btn.itemNumber = index;
//
//       wrapper.appendChild(btnWrp);
//
//       btn.addEventListener('click', function (e) {
//         toggleSpoiler(items[e.target.itemNumber]);
//       });
//
//     }
//
//   }
//
//   function toggleSpoiler(el) {
//     var body = el.querySelector('.' + bodyClass);
//     var btn = el.querySelector('.' + spoilerBtnClass);
//     var state = el.spoilerState;
//
//     if (state === 'open') {
//       btn.innerHTML = 'Что включено';
//       body.classList.remove('-open');
//       body.classList.add('-close');
//       el.spoilerState = 'close';
//     } else if (state === 'close') {
//       btn.innerHTML = 'Скрыть описание';
//       body.classList.remove('-close');
//       body.classList.add('-open');
//       el.spoilerState = 'open';
//     } else {
//       console.log('Состояние элемента не опреледено');
//     }
//
//   }
// }
//
// function polling() {
//   var item = document.querySelectorAll('.polling');
//   var title = document.querySelectorAll('.polling__title');
//
//   // вешаем обработчик на клик
//   for (var i = 0; i < title.length; i++) {
//     title[i].addEventListener('click', function (e) {
//       toggle(e.target.parentElement);
//     });
//   }
//
//   // скрываем все пункты кроме первого
//   for (i = 0; i < item.length; i++) {
//     if (i === 0) showByStart(item[i], i); else hideByStart(item[i], i);
//   }
//
//   function toggle(el) {
//     var state = el.spoilerState;
//     var body = el.querySelector('.polling__main');
//     var bodyMaxHeight = body.maxHeight;
//     var index = el.index;
//
//     if (state === 'open') {
//
//       el.spoilerState = 'close';
//       body.classList.add('-close');
//       body.classList.remove('-open');
//       body.style.maxHeight = 0;
//
//     } else if (state === 'close') {
//
//       closeOther(index);
//
//       el.spoilerState = 'open';
//       body.classList.add('-open');
//       body.classList.remove('-close');
//       body.style.maxHeight = bodyMaxHeight + 'px';
//
//     } else {
//       console.error('Не могу переключить. Неизвестное состояние.')
//     }
//   }
//
//   function hideByStart(el, index) {
//     var body = el.querySelector('.polling__main');
//     el.spoilerState = 'close';
//     el.index = index;
//     body.maxHeight = body.offsetHeight;
//     body.style.maxHeight = 0;
//     body.classList.add('-close');
//   }
//
//   function showByStart(el, index) {
//     var body = el.querySelector('.polling__main');
//     el.spoilerState = 'open';
//     el.index = index;
//     body.maxHeight = body.offsetHeight;
//     body.style.maxHeight = body.maxHeight;
//     body.classList.add('-open');
//   }
//
//   function closeOther(index) {
//     for (i = 0; i < item.length; i++) {
//       var el = item[i];
//       var body = el.querySelector('.polling__main');
//
//       if (i !== index) {
//
//         el.spoilerState = 'close';
//         body.classList.add('-close');
//         body.classList.remove('-open');
//         body.style.maxHeight = 0;
//
//       }
//
//     }
//   }
//
// }
//
// document.addEventListener('DOMContentLoaded', aboutSpeakers());
//
// document.addEventListener('DOMContentLoaded', tariffs());
//
// document.addEventListener('DOMContentLoaded', polling());
//
