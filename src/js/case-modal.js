document.addEventListener('DOMContentLoaded', function () {

  'use strict';

  console.log('INIT bars.js');

  var caseLink = document.querySelectorAll('.slider__item');



  $('[data-popup-case]').magnificPopup({
    type:'inline',
    midClick: true,
    callbacks: {
      open: function () {

        setBars(this.currItem.inlineElement[0]);

      },
      close: function () {
        unsetBars(this.currItem.inlineElement[0]);
      }
    }
  });

  initBars();

  createNumbers();

  createImageGallery();

});


function initBars() {
  // выставляем прогрессбары в начальное
  var bars = document.querySelectorAll('.caseSidebar__bar');

  for (var i = 0; i < bars.length; i++) {

    bars[i].classList.remove('nojs');
    bars[i].style.width = '0%';

  }

  console.log('f: initBars');
}


function setBars(caseEl) {
  // выставляем прогрессбары в соответствии с атрибутом
  var bars = caseEl.querySelectorAll('.caseSidebar__bar');


  setTimeout(function () {
    for (var i = 0; i < bars.length; i++) {

      bars[i].style.width = bars[i].getAttribute('data-bar-position') + '%';

    }
  }, 400);



  console.log('f: setBars');
}


function unsetBars(caseEl) {
  // выставляем прогрессбары в соответствии с атрибутом
  var bars = caseEl.querySelectorAll('.caseSidebar__bar');

  for (var i = 0; i < bars.length; i++) {

    bars[i].style.width = '';

  }

  console.log('f: unsetBars');
}



function createNumbers() {
  // добавляем нули к номерам меньше 10
  var lists = document.querySelectorAll('.caseList__list');
  var rows = document.querySelectorAll('.caseList__row');

  for (var i = 0; i < lists.length; i++) {

    var rows = lists[i].querySelectorAll('.caseList__row');

    lists[i].classList.remove('nojs');

    for (var j = 0; j < rows.length; j++) {
      var row = rows[j];
      var currentNumber = j < 9 ? '0' + (j+1).toString() : j+1;
      var number = document.createElement('div');
      number.className = 'caseList__number';
      number.innerHTML = currentNumber;

      row.insertBefore(number, row.firstChild);

    }

  }

}


function createImageGallery() {

  var gallery = tns({
    container: '.gallery__main',
    items: 1,
    slideBy: 1,
    controls: false,
    mode: 'gallery',
    navContainer: '.gallery__thumbnail',
    navAsThumbnails: true,
    onInit: function () {
      var sliderWrapper = document.querySelector('.gallery');
      sliderWrapper.classList.remove('nojs');
    }
  });

}