document.addEventListener("DOMContentLoaded", function () {
  'use strict';

  // 02 Оболочка
  var shellSlider = tns({
    container: '.tabs__tabsContent',
    items: 1,
    slideBy: 'page',
    controlsContainer: '.tabs__nav',
    navContainer: '.tabs__list',
  });

  sliderCounter();


  // Наши кейсы
  var caseSliderTop = tns({
    container: '.slider__listTop',
    items: 2,
    slideBy: 1,
    nav: false,
    controlsContainer: '.cases__arrows',
    responsive: {
      759: {
        items: 3,
        slideBy: 2
      }
    }
  });
  var caseSliderBottom = tns({
    container: '.slider__listBottom',
    items: 2,
    slideBy: 2,
    nav: false,
    controlsContainer: '.cases__arrows',
    responsive: {
      759: {
        items: 3,
        slideBy: 3
      }
    }
  });

  console.log('INIT slider.js');
});

function sliderCounter() {
  console.log('f: sliderCounter');

  var nextBtn = document.querySelector('.tabs__button--next');
  var prevBtn = document.querySelector('.tabs__button--prev');
  var currentValue = document.querySelector('.tabs__current');
  var totalValue = document.querySelector('.tabs__total');
  var total = document.querySelector('.tabs__list').querySelectorAll('.tabs__tab').length;
  var current = 1;

  totalValue.innerHTML = total;
  current.innerHTML = current;



  nextBtn.addEventListener('click', function () {

    if (current === total) {
      current = 1;
    } else {
      current++;
    }

    currentValue.innerHTML = current;

  });


  prevBtn.addEventListener('click', function () {

    if (current === 1) {
      current = 8;
    } else {
      current--;
    }

    currentValue.innerHTML = current;

  });


}