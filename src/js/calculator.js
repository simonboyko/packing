document.addEventListener("DOMContentLoaded", function () {
  'use strict';

  var value1 = 0;
  var value2 = 0;
  var value3 = 0;

  // Клиентов в месяц
  var clientSlider = document.querySelector('.calculator__range--clients');
  var clientSliderValue = document.querySelector('.calculator__value--clients');

  noUiSlider.create(clientSlider, {
    connect: [true, false],
    start: [ 21 ],
    step: 1,
    range: {
      'min': [ 1 ],
      'max': [ 100 ]
    },
    format: wNumb({
      decimals: 0
    })
  });

  clientSlider.noUiSlider.on('update', function( values, handle, unencoded ) {
    clientSliderValue.innerHTML = values[handle];
    value1 = unencoded;
  });



  // средний чек
  var billSlider = document.querySelector('.calculator__range--bill');
  var billSliderValue = document.querySelector('.calculator__value--bill');

  noUiSlider.create(billSlider, {
    connect: [true, false],
    start: [ 1000 ],
    step: 1000,
    range: {
      'min': [ 1000 ],
      'max': [ 1000000 ]
    },
    format: wNumb({
      decimals: 0,
      thousand: ' ',
      suffix: ' руб.'
    })
  });

  billSlider.noUiSlider.on('update', function( values, handle, unencoded ) {
    billSliderValue.innerHTML = values[handle];
    value2 = unencoded;
  });


  // период
  var periodSlider = document.querySelector('.calculator__range--period');
  var periodSliderValue = document.querySelector('.calculator__value--period');

  noUiSlider.create(periodSlider, {
    connect: [true, false],
    start: [ 1 ],
    step: 1,
    range: {
      'min': [ 1 ],
      'max': [ 24 ]
    },
    format: wNumb({
      decimals: 0,
      suffix: ' мес.'
    })
  });

  periodSlider.noUiSlider.on('update', function(values, handle, unencoded) {
    periodSliderValue.innerHTML = values[handle];
    value3 = unencoded;
  });

  checkResult();

  updateResult();

  function checkResult() {

    clientSlider.noUiSlider.on('update', function(values, handle, unencoded) {
      updateResult();
    });

    billSlider.noUiSlider.on('update', function(values, handle, unencoded) {
      updateResult();
    });

    periodSlider.noUiSlider.on('update', function(values, handle, unencoded) {
      updateResult();
    });
  }

  function updateResult() {

    var result = document.querySelector('.calculator__result');
    var Format = wNumb({
      decimals: 0,
      thousand: ' '
    });

    result.innerHTML = (Format.to(value1 * value2 * value3)).toString();

  };

  console.log('INIT calculator.js');
});




